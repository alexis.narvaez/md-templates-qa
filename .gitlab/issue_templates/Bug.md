### Resumen

(Resuma el error encontrado de manera concisa)

### Pasos para reproducir el error

(Cómo se puede reproducir el problema - **ESTO ES MUY IMPORTANTE**)

### ¿Cuál es el comportamiento actual del error?

(Lo que realmente pasa)

### ¿Cuál es el comportamiento correcto esperado?

(Lo que deberías ver en su lugar)

### Registros y/o capturas de pantalla relevantes

(Pegue los registros relevantes; use bloques de código (`` `) para formatear la
consola salida, registros y código, ya que de lo contrario es muy difícil de
leer).

**Recomendamos adjuntar archivo `.har` que se permite (generar desde Chrome o
Firefox)[https://www.linkedin.com/help/linkedin/answer/98234/creacion-de-un-archivo-har?lang=es]
en caso de que el issue sea de frontend**

Si estaba realizando una solicitud (backend), escriba el método (` GET`` POST `
` PUT`` PATCH ` `DELETE`) y la ruta del `endpoint`. Por ejemplo: `/login`. usado

Si la solicitud de respuesta es un `JSON`, formatee el objeto en
https://jsonformatter.org/

**`Nombre de la solicitud aquí`**

Si el método utilizado contiene un cuerpo en la solicitud, agréguelo por favor

```
  Logs here,
  Body request
```

**Ejemplo**

**`POST /login`**

```
{
  "email": "example@example.com",
  "password": "password"
}
```

### Posibles correcciones

(Si puede, enlace a la línea de código que podría ser responsable del problema)

/label ~bug ~reproduced ~needs-investigation