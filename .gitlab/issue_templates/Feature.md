### Resumen

(Resuma la nueva funcionalidad)

### Requerimientos

(Agregar que debe cumplir dicha funcionalidad)

Ejemplo

- [x] Validar el correo electrónico
- [ ] Arrojar mensajes de error según los casos de la documentación

### Capturas del diseño

(En caso de aplicar, agregar imágenes relevantes para la nueva funcionalidad)

### Links / Referencias

(Agregar links de la documentación que se relaciona a esta nueva funcionalidad)

/label ~feature ~suggestion